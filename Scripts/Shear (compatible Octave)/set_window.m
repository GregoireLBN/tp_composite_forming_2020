function [] = set_window(x_label, y_label, Title_label)
figure; hold on; set(gca,'FontSize',14); set(gca,'FontName','Times'); set(gcf,'Color',[1,1,1]);
set(gca,'XMinorTick','on','YMinorTick','on')
xlabel(x_label);ylabel(y_label);title( Title_label);
% grid(gca,'major')
% set(gca,'GridLineStyle','--')

% Windows size and format
 width=600;
 num_or = (1+sqrt(5))/2;
 height=width;
 set(0,'Units','pixels')
 borders = get(gcf,'OuterPosition') - get(gcf,'Position');
 tailleecran=get(0,'ScreenSize');
 widthout=width+2*abs(borders(1));
 heightout=height+abs(borders(4))-abs(borders(1));
 set(gcf,'units','pixels','position',[(tailleecran(3)-widthout)/2.0,(tailleecran(4)-heightout)/2.0,width,height])
 grid on
end