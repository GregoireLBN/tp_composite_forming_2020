% @Brief  : Write a simple vtk mesh 
% @Author : Eduardo Guzman Maldonado
% @Copyrigth: Innovamics

function VTK_TRI_writer(coord,lnods,filename)
% VTK file format:
% # vtk DataFile Version 3.0              >> Header
% 1D Finite element analysis- clampedbar  >> Title
% ASCII                                   >> Data type
% DATASET UNSTRUCTURED_GRID               >> Geometrie Topology
% POINTS  n datatype                      >> Dataset attributes  
% x0   y0   z0
% x1   y1   z1
% x2   y2   z2
% xn-1 yn-1 zn-1
% CELLS  n size
% numpoints  p1 p2 p3 ...
% CELLS TYPES n
% type
% type
% type

npoints = numel(coord(:,1));
nelemen = numel(lnods(:,1));

file_vtk = filename;

%% VTK writing
 fid = fopen(file_vtk, 'w');
if fid == -1
    error('Cannot open file for writing.');
end
  
fprintf(fid,'# vtk DataFile Version 3.0\n');
fprintf(fid,'Mesh visualiation\n');
fprintf(fid,'ASCII\n');
fprintf(fid,'DATASET UNSTRUCTURED_GRID\n');
fprintf(fid,'POINTS %5d float\n',npoints+1);
fprintf(fid,'%3.7f   %3.7f   %3.7f \n',0, 0, 0);
fprintf(fid,'%3.7f   %3.7f   %3.7f \n',coord');
fprintf(fid,'CELLS %d %d \n',nelemen,nelemen*4);
fprintf(fid,'3 %5d %5d %5d  \n',lnods' );
fprintf(fid,'CELL_TYPES  %d \n',nelemen);
fprintf(fid,'%d \n',ones(1,nelemen)*5 );
fclose(fid);
end